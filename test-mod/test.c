#include <stdio.h>
#include <autopatch/autopatch.h>

int patch_test(void) {
  printf("%d\n", ORIG(int, (void), ()));

  puts ("Goodbye, cruel world.");
  return 42;
}