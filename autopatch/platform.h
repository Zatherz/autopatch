#ifndef H_AUTOPATCH_PLATFORM
#define H_AUTOPATCH_PLATFORM
#include <platform/common.h>
#include <platform/api.h>

#ifdef __linux__
#include <platform/linux.h>
#define AUTOPATCH_PLATFORM_NAME "Linux"
/*
#elif PLATFORM_SPECIFIC_DEFINE
#include <platform/PLATFORM_NAME.h>
*/
#else
#error "Unsupported platform!"

#endif
#endif//H_AUTOPATCH_PLATFORM
