#include <subhook/subhook.h>
#include <libanalog/analog.h>
#ifndef H_AUTOPATCH
#define H_AUTOPATCH

#define AUTOPATCH_VERSION "0.4"

#define PREFIX_PATCH "patch_"
#define PREFIX_PATCH_ADDRESS "patchaddr_"
#define PREFIX_ORIG "orig_"

struct patch {
  subhook_t hook;
  size_t target_addr;
};

void AUTOPATCH_patch_remove(char* patch_func_name);
void AUTOPATCH_patch_install(char* patch_func_name);
struct patch* AUTOPATCH_patch_get(char* patch_func_name);
void* AUTOPATCH_patch_get_func(char* patch_func_name);

#define PATCH_REMOVE() AUTOPATCH_patch_remove((char*)__func__)
#define PATCH_INSTALL() AUTOPATCH_patch_install((char*)__func__)
#define PATCH_GET() AUTOPATCH_patch_get((char*)__func__)
#define PATCH_GET_FUNC() AUTOPATCH_patch_get_func((char*)__func__)

#define ORIG_VOID(param_types, args) ({ \
  AUTOPATCH_patch_remove((char*)__func__); \
  ((ret_type (*)param_types)AUTOPATCH_patch_get_func((char*)__func__)) args;\
  AUTOPATCH_patch_install((char*)__func__);\
})

#define ORIG(ret_type, param_types, args) ({ \
  AUTOPATCH_patch_remove((char*)__func__); \
  ret_type return_ = ((ret_type (*)param_types)AUTOPATCH_patch_get_func((char*)__func__)) args;\
  AUTOPATCH_patch_install((char*)__func__);\
  return_; \
})

#ifdef AUTOPATCH_SOURCE
#include <platform.h>

#define AUTOPATCH_CTOR_FUNCNAME AUTOPATCH_constructor
#define AUTOPATCH_DTOR_FUNCNAME AUTOPATCH_destructor

#define AUTOPATCH_CTOR AUTOPATCH_PLATFORM_CTOR
#define AUTOPATCH_DTOR AUTOPATCH_PLATFORM_DTOR

#define AUTOPATCH_DEBUG_VALUE(str) ANALOG_COLOR_FG_BRIGHT_YELLOW(str)
#define AUTOPATCH_DEBUG_TOP_LEVEL_GOAL(str) ANALOG_COLOR_FG_MAGENTA(str)
#define AUTOPATCH_DEBUG_SUCCESS(str) ANALOG_COLOR_FG_BRIGHT_GREEN(str)
#endif

typedef enum autopatch_flags {
	AUTOPATCH_FLAG_DEBUG     = 1 << 0,
	AUTOPATCH_FLAG_DETAIL    = 1 << 1,
	AUTOPATCH_FLAG_SILENT    = 1 << 2,
	AUTOPATCH_FLAG_NO_ERRORS = 1 << 3
} autopatch_flags_t;

ANALOG_DECLARE_LOG_TYPE(debug);
ANALOG_DECLARE_LOG_TYPE(error);
ANALOG_DECLARE_LOG_TYPE(tip);
ANALOG_DECLARE_LOG_TYPE(continuity);

#endif//H_AUTOPATCH
