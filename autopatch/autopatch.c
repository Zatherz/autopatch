#define AUTOPATCH_SOURCE

#include <stdio.h>
#include <autopatch.h>
#include <subhook/subhook.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <c_hashmap/hashmap.h>
#include <stdarg.h>
#include <platform.h>
#include <libasymtab/asymtab.h>
#include <libanalog/analog.h>
#include <libanalog/akit_colors.h>

/*
  Used environment variables:
  * AUTOPATCH_DISABLED
  * AUTOPATCH_OPTIONS
  * AUTOPATCH_LIBS [req]
  * AUTOPATCH_SYMTAB [req]
*/

#ifdef BIT64
#define SUBHOOK_BITS 64
#endif

static map_t AUTOPATCH_INTERNAL_patch_map;
static bool AUTOPATCH_INTERNAL_initialized = false;

static asymtab_t* AUTOPATCH_INTERNAL_symtab;

static autopatch_flags_t AUTOPATCH_INTERNAL_flags = 0;

ANALOG_DEFINE_LOG_TYPE(debug, ANALOG_COLOR_DEBUG, ANALOG_COLOR_DEBUG_TEXT);
ANALOG_DEFINE_LOG_TYPE(error, ANALOG_COLOR_ERROR, ANALOG_COLOR_ERROR_TEXT);
ANALOG_DEFINE_LOG_TYPE(tip, ANALOG_COLOR_TIP, ANALOG_COLOR_TIP_TEXT);
ANALOG_DEFINE_LOG_TYPE(continuity, ANALOG_ANSI_FG_BRIGHT_GREEN, ANALOG_ANSI_DEFAULT);

void AUTOPATCH_patch_remove(char* patch_func_name) {
  any_t val;
  int result = hashmap_get(AUTOPATCH_INTERNAL_patch_map, patch_func_name, &val);
  if (result == MAP_MISSING) return;
  subhook_remove(((struct patch*)val)->hook);
}

void AUTOPATCH_patch_install(char* patch_func_name) {
  any_t val;
  int result = hashmap_get(AUTOPATCH_INTERNAL_patch_map, patch_func_name, &val);
  if (result == MAP_MISSING) return;
  subhook_install(((struct patch*)val)->hook);
}

struct patch* AUTOPATCH_patch_get(char* patch_func_name) {
  any_t val;
  int result = hashmap_get(AUTOPATCH_INTERNAL_patch_map, patch_func_name, &val);
  if (result == MAP_MISSING) return NULL;
  return (struct patch*)val;
}

void* AUTOPATCH_patch_get_func(char* patch_func_name) {
  return (void*)(AUTOPATCH_patch_get(patch_func_name)->target_addr);
}

int AUTOPATCH_INTERNAL_free_patch_map(any_t item, any_t data) {
  struct patch* patch = (struct patch*)data;
  subhook_remove(patch->hook);
  subhook_free(patch->hook);
  return MAP_OK;
}

bool AUTOPATCH_INTERNAL_log_filter(char* log_type) {
  if (AUTOPATCH_INTERNAL_flags & AUTOPATCH_FLAG_SILENT) return false;

  if (strcmp(log_type, "debug") == 0) return (AUTOPATCH_INTERNAL_flags & AUTOPATCH_FLAG_DEBUG);
  else if (strcmp(log_type, "error") == 0) return !(AUTOPATCH_INTERNAL_flags & AUTOPATCH_FLAG_NO_ERRORS);

  return true;
}

void AUTOPATCH_INTERNAL_create_patch(char* name, void* target_ptr, void* patch_ptr) {
  ANALOGf_debug(">>> Creating patch for " ANALOG_COLOR_VALUE("'%s'") ". Target: " ANALOG_COLOR_VALUE("0x%zx") ", patch: " ANALOG_COLOR_VALUE("0x%zx") "\n", name, target_ptr, patch_ptr);
  subhook_t patch_subhook = subhook_new(target_ptr, patch_ptr, 
    #ifdef BIT64
      SUBHOOK_OPTION_64BIT_OFFSET
    #else
      0
    #endif
  );
  subhook_install(patch_subhook);

  struct patch* patch = malloc(sizeof(struct patch));
  patch->hook = patch_subhook;
  patch->target_addr = (size_t)target_ptr;

  hashmap_put(AUTOPATCH_INTERNAL_patch_map, name, patch);
}

void AUTOPATCH_INTERNAL_load_mod(char* path) {
  void* handle = AUTOPATCH_PLATFORM_load_lib(path);
  if (handle == NULL) {
    ANALOGf_error("Failed opening mod lib '%s'.\n", path);
    ANALOGf_continuity("Ignoring mod.\n");
    return;
  }

  struct func_list* funcs = AUTOPATCH_PLATFORM_get_functions_in(path);

  if (funcs == NULL) {
    ANALOGf_error("Mod library '%s' is stripped!\n", path);
    ANALOGf_continuity("Ignoring mod.\n");
    return;
  }

  for (int i = 0; i < funcs->size; i++) {
    struct func_list_entry* ent = funcs->entries[i];

    char* name = ent->name;
    void* ptr = AUTOPATCH_PLATFORM_get_lib_sym_ptr(handle, name);

    if (strncmp(PREFIX_PATCH_ADDRESS, name, strlen(PREFIX_PATCH_ADDRESS)) == 0) {
      ANALOGf_debug(">> " "Patch address function found: " ANALOG_COLOR_VALUE("'%s'") " @ " ANALOG_COLOR_VALUE("0x%zx") "\n", name, ptr);

      char* target_addr_str = name + strlen(PREFIX_PATCH_ADDRESS);
      size_t target_addr;
      sscanf(target_addr_str, "%zx", &target_addr);

      target_addr = AUTOPATCH_PLATFORM_get_exe_virtual_addr(target_addr);

      if (target_addr == 0) {
        ANALOGf_error("Failed converting physical address " ANALOG_COLOR_VALUE("0x%zx") " into a virtual address.\n");
        ANALOGf_continuity("Ignoring this patch function (" ANALOG_COLOR_VALUE("'%s'") ").\n", name);
        continue;
      }

      ANALOGf_debug(">> Target address: " ANALOG_COLOR_VALUE("0x%zx\n"), target_addr);
      AUTOPATCH_INTERNAL_create_patch(name, (void*)target_addr, ptr);
    } else if (strncmp(PREFIX_PATCH, name, strlen(PREFIX_PATCH)) == 0) {
      ANALOGf_debug(">> Patch function found: " ANALOG_COLOR_VALUE("'%s'") " @ " ANALOG_COLOR_VALUE("0x%zx") "\n", name, ptr);

      char* target = name + strlen(PREFIX_PATCH);
      ANALOGf_debug(">> Requesting asymtab symbol with name " ANALOG_COLOR_VALUE("'%s'") "\n", target);
      asymbol_t* sym = asymtab_symbol_by_name(AUTOPATCH_INTERNAL_symtab, target);
      if (sym == NULL) {
        ANALOGf_error("Symbol does not exist in the symbol table.\n");
        ANALOGf_tip("Is the right symbol table associated with this mod library?\n");
        ANALOGf_continuity("Ignoring this patch function (" ANALOG_COLOR_VALUE("'%s'") ").\n", name);
        continue;
      }
      size_t target_addr = AUTOPATCH_PLATFORM_get_exe_virtual_addr(sym->address);

      if (target_addr == 0) {
        ANALOGf_error("Failed converting physical address " ANALOG_COLOR_VALUE("0x%zx") " into a virtual address.\n");
        ANALOGf_continuity("Ignoring this patch function (" ANALOG_COLOR_VALUE("'%s'") ").\n", name);
        continue;
      }

      ANALOGf_debug(">> Target function: " ANALOG_COLOR_VALUE("'%s'") " @ " ANALOG_COLOR_VALUE("0x%zx") "\n", target, target_addr);
      AUTOPATCH_INTERNAL_create_patch(name, (void*)target_addr, ptr);
    }
  }
}

AUTOPATCH_CTOR {
  if (getenv("AUTOPATCH_DISABLED")) return;

  ANALOG_CONFIG_filter = AUTOPATCH_INTERNAL_log_filter;
  ANALOG_CONFIG_default_id = ANALOG_COLOR_FG_BRIGHT_BLACK("autopatch");

  char* options = getenv("AUTOPATCH_OPTIONS");

  char* delimiter = ":";
  char* result;

  if (options != NULL) {
    result = strtok(options, delimiter); 
    while (result != NULL) {
      if (strcmp(result, "debug") == 0) AUTOPATCH_INTERNAL_flags |= AUTOPATCH_FLAG_DEBUG;
      else if (strcmp(result, "detail") == 0) AUTOPATCH_INTERNAL_flags |= AUTOPATCH_FLAG_DETAIL;
      else if (strcmp(result, "no_errors") == 0) AUTOPATCH_INTERNAL_flags |= AUTOPATCH_FLAG_NO_ERRORS;
      else if (strcmp(result, "silent") == 0) AUTOPATCH_INTERNAL_flags |= AUTOPATCH_FLAG_SILENT;

      result = strtok(NULL, delimiter);
    }
    result = NULL;
  }

  ANALOGf_debug(ANALOG_ANSI_FG_BRIGHT_GREEN "AutoPatch v" AUTOPATCH_VERSION " successfully injected into target process.\n" ANALOG_ANSI_RESET);

  if (AUTOPATCH_INTERNAL_flags & AUTOPATCH_FLAG_DETAIL) {
    ANALOGf_debug("> Platform: " ANALOG_COLOR_VALUE(AUTOPATCH_PLATFORM_NAME) "\n");
    ANALOGf_debug("> Injected program name: '" ANALOG_COLOR_VALUE("%s") "'\n", AUTOPATCH_PLATFORM_ARGV[0]);
  }

  char* mods = getenv("AUTOPATCH_LIBS");
  if (mods == NULL) {
    ANALOGf_error("AUTOPATCH_LIBS not defined.\n");
    ANALOGf_continuity("AutoPatch will terminate. The target program will keep on running.\n");
    return;
  }

  char* symtab = getenv("AUTOPATCH_SYMTAB");
  if (symtab == NULL) {
    ANALOGf_error("AUTOPATCH_SYMTAB not defined.\n");
    ANALOGf_continuity("AutoPatch will terminate. The target program will keep on running.\n");
    return;
  }  

  ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Loading symbol table") "\n");
  AUTOPATCH_INTERNAL_symtab = asymtab_read(symtab);

  if (AUTOPATCH_INTERNAL_symtab == NULL) {
    ANALOGf_error("Failed loading symtab from '%s'.\n", symtab);
    ANALOGf_continuity("AutoPatch will terminate. The target program will keep on running.\n");
    return;
  }

  ANALOGf_debug("> " ANALOG_COLOR_SUCCESS("Loaded with "SIZE_T_FORMAT" symbols.") "\n", AUTOPATCH_INTERNAL_symtab->symbols_len);
  if (AUTOPATCH_INTERNAL_flags & AUTOPATCH_FLAG_DETAIL) {
    ANALOGf_debug("> Entries:\n");

    for (int i = 0; i < AUTOPATCH_INTERNAL_symtab->symbols_len; i++) {
      asymbol_t* sym = AUTOPATCH_INTERNAL_symtab->symbols[i];

      ANALOGf_debug(">> %s = " ANALOG_COLOR_VALUE("0x%zx") "\n", sym->name, sym->address);
      ANALOGf_debug(">>> %s\n", sym->description);
    }
  }

  ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Loading mod libraries") "\n");

  AUTOPATCH_INTERNAL_patch_map = hashmap_new();

  result = strtok(mods, delimiter);
  while (result != NULL) {
    ANALOGf_debug("> Loading mod library at " ANALOG_COLOR_VALUE("'%s'") "\n", result);

    AUTOPATCH_INTERNAL_load_mod(result);

    result = strtok(NULL, delimiter);
  }

  AUTOPATCH_INTERNAL_initialized = true;
}

AUTOPATCH_DTOR {
  if (AUTOPATCH_INTERNAL_initialized) {
    ANALOGf_debug("Cleaning up hashmap\n");

    hashmap_iterate(AUTOPATCH_INTERNAL_patch_map, AUTOPATCH_INTERNAL_free_patch_map, NULL);
    hashmap_free(AUTOPATCH_INTERNAL_patch_map);
  }

  ANALOGf_debug("Bye!\n");
}
