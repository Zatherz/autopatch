#include <platform.h>
#include <string.h>

map_t AUTOPATCH_PLATFORM_INTERNAL_cached_functions = NULL;

void AUTOPATCH_PLATFORM_INTERNAL_init_cached_functions(void) {
  if (AUTOPATCH_PLATFORM_INTERNAL_cached_functions == NULL) {
    AUTOPATCH_PLATFORM_INTERNAL_cached_functions = hashmap_new();
  }
}

struct func_list* AUTOPATCH_PLATFORM_cached_get_functions_in(char* path) {
  AUTOPATCH_PLATFORM_INTERNAL_init_cached_functions();

  any_t cached;
  if (hashmap_get(AUTOPATCH_PLATFORM_INTERNAL_cached_functions, path, &cached) == MAP_OK) {
    return (struct func_list*)cached;
  }

  struct func_list* list = AUTOPATCH_PLATFORM_get_functions_in(path);
  hashmap_put(AUTOPATCH_PLATFORM_INTERNAL_cached_functions, path, list);

  return list;
}

size_t AUTOPATCH_PLATFORM_get_function_addr_in(char* path, char* name) {
  struct func_list* list = AUTOPATCH_PLATFORM_cached_get_functions_in(path);
  if (list == NULL) return 0;

  for (int i = 0; i < list->size; i++) {
    struct func_list_entry* ent = list->entries[i];

    if (strcmp(ent->name, name) == 0) {
      return ent->addr;
    }
  }

  return 0;
}
