cmake_minimum_required(VERSION 3.10)
project(autopatch)

file (GLOB autopatch_src *.c)
file (GLOB c_hashmap_src "${CMAKE_SOURCE_DIR}/libs/c_hashmap/*.c")
file (GLOB subhook_src "${CMAKE_SOURCE_DIR}/libs/subhook/subhook.c")

if("${PLATFORM}" STREQUAL "Linux")
	set (platform_src platform/linux.c)
	set (platform_libs elf dl)
else()
	message(SEND_ERROR "Unknown platform: '${PLATFORM}'")
endif()

include_directories(. ../libs ../libs/libasymtab ../libs/libasymtab/libs ../libs/libanalog)
link_directories("${CMAKE_BINARY_DIR}/libs/libasymtab/libasymtab")

add_library(
	autopatch SHARED
	${autopatch_src} ${platform_src}
	${c_hashmap_src} ${subhook_src}
)

target_link_libraries(autopatch libasymtab libanalog sds ${platform_libs})