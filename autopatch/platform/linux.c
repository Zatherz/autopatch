#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <elf.h>
#include <libelf.h>
#include <stdlib.h>
#include <string.h>
#include <platform/linux.h>
#include <c_hashmap/hashmap.h>
#include <platform/common.h>
#include <dlfcn.h>
#include <stdbool.h>

static bool AUTOPATCH_PLATFORM_INTERNAL_base_addr_set = false;
static size_t AUTOPATCH_PLATFORM_INTERNAL_base_addr = -1;

void* AUTOPATCH_PLATFORM_load_lib(char* path) {
  return dlopen(path, RTLD_NOW | RTLD_GLOBAL);
}

void* AUTOPATCH_PLATFORM_get_lib_sym_ptr(void* lib, char* sym) {
  return dlsym(lib, sym);
}

size_t AUTOPATCH_PLATFORM_get_exe_virtual_addr(size_t physical_addr) {
  if (!AUTOPATCH_PLATFORM_INTERNAL_base_addr_set) {
    AUTOPATCH_PLATFORM_INTERNAL_base_addr_set = true;
    AUTOPATCH_PLATFORM_INTERNAL_link_map_t* map = dlopen(NULL, RTLD_NOW);
    AUTOPATCH_PLATFORM_INTERNAL_base_addr = (size_t)map->base_address;
    dlclose(map);
  }

  return physical_addr + AUTOPATCH_PLATFORM_INTERNAL_base_addr;
}

struct func_list* AUTOPATCH_PLATFORM_get_functions_in(char* path) {
  // open the executable
  // and get the file descriptor
  int exe_fd = open(path, O_RDONLY);
  if (exe_fd == -1) {
    fprintf(stderr, "Failed opening ELF '%s'\n", path);
    exit(EXIT_FAILURE);
  }

  // set elf version and read in the elf file
  // from the file descriptor
  elf_version(EV_CURRENT);
  Elf* elf = elf_begin(exe_fd, ELF_C_READ, NULL);
  if (elf == NULL) {
    fprintf(stderr, "Failed loading the current executable as ELF\n");
    exit(EXIT_FAILURE);
  }

  // load in elf sections
  ElfX_Ehdr* header = elfx_getehdr(elf);
  if (header == NULL) {
    fprintf(stderr, "Failed loading ELF header\n");
    exit(EXIT_FAILURE);
  }

  Elf_Scn* section = elf_getscn(elf, header->e_shstrndx);
  if (section == NULL) {
    fprintf(stderr, "Failed loading ELF section info\n");
    exit(EXIT_FAILURE);
  }

  Elf_Data* section_data = elf_getdata(section, NULL);
  if (section_data == NULL) {
    fprintf(stderr, "Failed loading ELF section data\n");
    exit(EXIT_FAILURE);
  }

  ElfX_Shdr* section_header;

  section = NULL;
  while ((section = elf_nextscn(elf, section)) != NULL) {
    section_header = elfx_getshdr(section);

    if (section_header == NULL) {
      fprintf(stderr, "Failed loading ELF section header\n");
      exit(EXIT_FAILURE);
    }

    // if this section is a symbol table...
    if (section_header->sh_type == SHT_SYMTAB) {
      Elf_Data* symtable_data = elf_getdata(section, NULL);
      if (symtable_data == NULL) {
        fprintf(stderr, "Couldn't retrieve symbol table data\n");
        exit(EXIT_FAILURE);
      }
      if (symtable_data->d_size == 0) {
        fprintf(stderr, "No data in symbol table\n");
        exit(EXIT_FAILURE);
      }

      ElfX_Sym* symbol = symtable_data->d_buf;
      ElfX_Sym* last_symbol = symtable_data->d_buf + symtable_data->d_size;

      char* sym_name;

      size_t ary_max_size = 10;
      size_t ary_ind = 0;
      struct func_list_entry** ary = malloc(sizeof(struct func_list_entry*) * ary_max_size);

      while ((symbol++) < last_symbol) {
        if (symbol->st_value == 0 ||
            ELFX_ST_BIND(symbol->st_info) == STB_WEAK ||
            ELFX_ST_BIND(symbol->st_info) == STB_NUM ||
            ELFX_ST_TYPE(symbol->st_info) != STT_FUNC) {
          continue;
        }

        sym_name = elf_strptr(elf, section_header->sh_link , (size_t)symbol->st_name);
        if (sym_name == NULL) {
          fprintf(stderr, "Failed getting symbol name\n");
          exit(EXIT_FAILURE);
        }

        struct func_list_entry* ent = malloc(sizeof(struct func_list_entry));
        ent->name = sym_name;
        ent->addr = AUTOPATCH_PLATFORM_get_exe_virtual_addr(symbol->st_value);

        if (ary_ind >= ary_max_size) {
          ary_max_size += 10;
          ary = realloc(ary, sizeof(struct func_list_entry*) * ary_max_size);
          if (ary == NULL) {
            fprintf(stderr, "Failed reallocating memory\n");
            exit(EXIT_FAILURE);
          }
        }

        ary[ary_ind] = ent;

        ary_ind ++;
      }

      struct func_list* f_list = malloc(sizeof(struct func_list));
      f_list->size = ary_ind;
      f_list->entries = ary;

      return f_list;
    }
  }

  return NULL;
}