#include <stdlib.h>
#include <c_hashmap/hashmap.h>

#ifndef H_PLATFORM_COMMON
#define H_PLATFORM_COMMON

struct AUTOPATCH_INTERNAL__link_map
{
   void*    base_address;   /* Base address of the shared object */
   char*    path;           /* Absolute file name (path) of the shared object */
   void*    not_needed1;    /* Pointer to the dynamic section of the shared object */
   struct AUTOPATCH_INTERNAL__link_map *next, *prev;/* chain of loaded objects */
};

struct func_list_entry {
  char* name;
  size_t addr;
};

// struct for string list with size
struct func_list {
  size_t size;
  struct func_list_entry** entries;
};

// cached wrapper around PLATFORM_get_functions_in(char* path);
struct func_list* PLATFORM_cached_get_functions_in(char* path);

// get address of symbol in executable
size_t PLATFORM_get_function_addr_in(char* path, char* name);
#endif//H_PLATFORM_COMMON
