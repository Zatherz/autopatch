#include <platform/common.h>
#ifndef H_PLATFORM_LINUX
#define H_PLATFORM_LINUX

#define AUTOPATCH_PLATFORM_CTOR static void AUTOPATCH_CTOR_FUNCNAME(int, char**);\
                                __attribute__((section(".init_array"))) static void* AUTOPATCH_PLATFORM_INTERNAL_ctor = AUTOPATCH_CTOR_FUNCNAME;\
                                static void AUTOPATCH_CTOR_FUNCNAME(int AUTOPATCH_PLATFORM_INTERNAL_argc, char** AUTOPATCH_PLATFORM_INTERNAL_argv)

#define AUTOPATCH_PLATFORM_DTOR __attribute__((destructor)) static void AUTOPATCH_DTOR_FUNCNAME(void)

#define AUTOPATCH_PLATFORM_ARGV AUTOPATCH_PLATFORM_INTERNAL_argv
#define AUTOPATCH_PLATFORM_ARGC AUTOPATCH_PLATFORM_INTERNAL_argc

typedef struct AUTOPATCH_PLATFORM_INTERNAL_link_map {
   void*    base_address;   /* Base address of the shared object */
   char*    path;           /* Absolute file name (path) of the shared object */
   void*    not_needed1;    /* Pointer to the dynamic section of the shared object */
   struct AUTOPATCH_INTERNAL__link_map *next, *prev;/* chain of loaded objects */
} AUTOPATCH_PLATFORM_INTERNAL_link_map_t;

#if __GNUC__
  #if __x86_64__ || __ppc64__
    #define BIT64
  #else
    #define BIT32
  #endif
#elif !defined(__x86_64__) && !defined(__ppc64__) && !defined(BIT64) && !defined(BIT32)
  #error "Unknown integer size (32 or 64 bit) - please use -DBIT64 or -DBIT32"
#endif

#ifdef BIT64
#define ElfX_Shdr Elf64_Shdr
#define ElfX_Ehdr Elf64_Ehdr
#define ElfX_Phdr Elf64_Phdr
#define ElfX_Sym Elf64_Sym

#define ELFX_ST_BIND ELF64_ST_BIND
#define ELFX_ST_TYPE ELF64_ST_TYPE

#define elfx_getshdr elf64_getshdr
#define elfx_getehdr elf64_getehdr
#define elfx_getphdr elf64_getphdr
#else
#define ElfX_Shdr Elf32_Shdr
#define ElfX_Ehdr Elf32_Ehdr
#define ElfX_Phdr Elf32_Phdr
#define ElfX_Sym Elf32_Sym

#define ELFX_ST_BIND ELF32_ST_BIND
#define ELFX_ST_TYPE ELF32_ST_TYPE

#define elfx_getshdr elf32_getshdr
#define elfx_getehdr elf32_getehdr
#define elfx_getphdr elf32_getphdr
#endif

#endif//H_PLATFORM_LINUX
