#include <stdlib.h>
#ifndef H_PLATFORM_API
#define H_PLATFORM_API
#include <autopatch.h>

////////////////////// PLATFORM API //////////////////////
// This is what platform specific code should implement //
//////////////////////////////////////////////////////////


/*
 * DEFINE THESE IN THE HEADER (.h) FILE (THE ONE THAT platform.h LOADS)
 */

// define the macro that's used to define the constructor
// (see linux.h for an example, use AUTOPATCH_CTOR_FUNCNAME for the name of the ctor function)
///// #define AUTOPATCH_PLATFORM_CTOR ...

// define the macro that's used to define the destructor
// (see linux.h for an example, use AUTOPATCH_DTOR_FUNCNAME for the name of the dtor function)
///// #define AUTOPATCH_PLATFORM_DTOR ...

// define the macro that's used to retrieve the commandline arguments (see linux.h for an example)
///// #define AUTOPATCH_PLATFORM_ARGV (...)

// define the macro that's used to retrieve the amount of commandline arguments (see linux.h for an example)
///// #define AUTOPATCH_PLATFORM_ARGC (...)

/*
 * DEFINE THESE IN THE SOURCE CODE (.c) FILE (THE ONE THAT IS ADDED AS A SOURCE FILE IN SConscript)
 */ 

// load shared object at runtime
void* AUTOPATCH_PLATFORM_load_lib(char* path);

// get address of symbol in shared object previously loaded by PLATFORM_load_lib
void* AUTOPATCH_PLATFORM_get_lib_sym_ptr(void* lib, char* sym);

// turn a physical address from the symtab into a usable virtual address (may work only for the executable)
// return 0 for failure
size_t AUTOPATCH_PLATFORM_get_exe_virtual_addr(size_t physical_addr);

// get address of symbol in *current* executable
size_t AUTOPATCH_PLATFORM_get_function_addr(char* name);

// get list of function names in library
// this will only be used for mod libraries
// platform code SHOULD NOT handle caching itself
// (that's the job of PLATFORM_cached_get_functions_in)
struct func_list* AUTOPATCH_PLATFORM_get_functions_in(char* path);

//////////////////////////////////////////////////////////

#endif//H_PLATFORM_API
