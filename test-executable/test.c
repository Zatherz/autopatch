#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

int main(void);

int test(void) {
  printf("Hello, world! test = 0x%zx, main = 0x%zx\n", (size_t)test, (size_t)main);
  return 69;
}

int main(void) {
  test();

  return 0;
}
