# AUTOPATCH WILL UNDERGO MAJOR CHANGES SOON. DO NOT USE THE CURRENT VERSION AS COMPATIBILITY WILL BE BROKEN SOON.

# AutoPatch 0.3

AutoPatch is a work in progress library for easy patching of native binaries.  
You preload `libautopatch.so`, add all your patch libs to `AUTOPATCH_LIBS`, and run the program.

As opposed to standard injection methods like `LD_PRELOAD`, it can patch functions from the executable (instead of just functions from shared libraries).

# Support

Currently, only Linux is supported (ELF binaries). However, porting is quite easy (and I'd be very happy to see contributions!). See below for more info.

# Patch format

AutoPatch loads every library in `AUTOPATCH_LIBS` (separated by colons [`:`]) and scans all functions inside them.  
When it finds a function with a name that starts with `patch_`, it will take this name, delete the `patch_` prefix, and use the resulting name as the "target" function name.

It will then scan through the running executable, find the target function, and use [subhook](github.com/Zeex/subhook) to patch the function in memory.

Any attempts to call the original function will result in the `patch_` function being called.

`patch_` functions will be ignored if the target function can not be found (for example, when operating on stripped binaries with no symbol table).

### `patchaddr_`

For cases where it's desirable to patch functions directly using addresses instead of names (e.g. stripped binaries), you may use `patchaddr_` functions.

When AutoPatch finds a function that starts with `patchaddr_` in a mod library, it will strip away `patchaddr_` and it will convert the rest into an address (it assumes that the rest of the name is in hex). Then, it will patch that address with the `patchaddr_` function.

The address should point to the first instruction of the function (its entry point).

`patchaddr_` functions will never be ignored, therefore it's very easy to cause a segmentation fault by using the wrong address. In case that happens, enabling debug mode (by setting the `AUTOPATCH_DEBUG` environment variable to anything) will help in locating which function causes the segfault.

### Original functions

AutoPatch provides utility macros (in the `autopatch.h` header) intended to be ran from inside `patch_` functions. One of those macros is `ORIG`, accepting three arguments in this format:

    RETURN_TYPE val = ORIG(RETURN_TYPE, (PARAM_TYPE_1, PARAM_TYPE_2), (PARAM_VALUE_1, PARAM_VALUE_2));

(Note: this makes use of GCC statement expressions, which may not be supported by all compilers)

For void functions, you have to use `ORIG_VOID` (due to the fact that there is no return value):

    ORIG_VOID((PARAM_TYPE_1, PARAM_TYPE_2), (PARAM_VALUE_1, PARAM_VALUE_2));

This will run the function from inside the running executable with the same name as the current function, but with `patch_` stripped away (so calling `ORIG` from inside `patch_test` will end up calling `test`). It's not as easy as that, though - it has to first uninstall the patch, call the function, and then reinstall the patch. *(Trampoline support coming soon)*



### Examples

```c
void patch_test(void) {
  ORIG_VOID((void), ());

  puts ("Hacked!");
}
// will patch function 'void test(void)' inside the executable
```

```c
char* patchaddr_4004f6(int a) {
  char* out = ORIG((char*), (int), (a));

  return "Hacked!";
}
// will patch function with entry point at 0x4004f6 inside the executable
```

# TODO

* Add a way to register patches manually (without using `patch_` autodetection, but still making use of automatic `free`ing)

* Windows support (OS X support would have to be added/tested in subhook first)

* Fix memory leaks

# Repository layout

* `autopatch` - AutoPatch source code
  
    * `libs` - Libraries used by AutoPatch

      * `c_hashmap` - [C hashmap implementation](https://github.com/petewarden/c_hashmap) (used for caching patches and function lists)

      * `subhook` - [Subhook](github.com/Zeex/subhook)

    * `platform` - Platform specific code

* `test` - Example target program

* `test_mod` - Example patch library

# Building

    scons

### Just AutoPatch

    scons autopatch

### Just test executable

    scons test-executable

### Just test mod library

    scons test-mod

# Invocation

AutoPatch needs to be forced to be loaded by an executable to start loading mods and patching functions. To do this, you will need to use a different method of dynamic library injection depending on your platform.

AutoPatch makes use of four environment variables for configuration:

* `AUTOPATCH_LIBS` - comma (`:`) separated list of paths to mod libraries
* `AUTOPATCH_DEBUG` - when set to anything, will enable verbose output
* `AUTOPATCH_SILENCE_ERRORS` - when set to anything, will hide errors
* `AUTOPATCH_DISABLED` - when set to anything, will cause AutoPatch to not do anything even if it's injected and `AUTOPATCH_LIBS` is defined

Below are OS-specific instructions on how to use AutoPatch.

### Linux

To inject AutoPatch, add `libautopatch.so` to `LD_PRELOAD`. Mod libraries should not be added to `LD_PRELOAD`, but only to `AUTOPATCH_LIBS`.

If the subhook library is not installed, you might have to add the path of `autopatch/libs/subhook` to `LD_LIBRARY_PATH`.

Example invocation (ran from the same directory as this readme file):

    env LD_PRELOAD="./autopatch/libautopatch.so" AUTOPATCH_LIBS="./test_mod/libtest.so" test/test

With debug mode:

    env LD_PRELOAD="./autopatch/libautopatch.so" AUTOPATCH_DEBUG=1 AUTOPATCH_LIBS="./test_mod/libtest.so" test/test

# Memory leaks

Valgrind reports a leak in the `get_functions_in` function.

# Porting

Porting AutoPatch to other systems is very easy (as long as the OS exposes API for interacting with dynamic libraries and executables).

`platform/api.h` defines a basic interface (there are only 4 functions!) that platform specific code must implement:

* `void* PLATFORM_load_lib(char* path);`

* `void* PLATFORM_get_lib_sym_ptr(void* lib, char* sym);`

* `size_t PLATFORM_get_function_addr(char* name);`

* `struct func_list* PLATFORM_get_functions_in(char* path);`

The header is commented (so you can check it out for more detailed information on what these are used for).

To start writing a platform backend, create the files `PLATFORMNAME.c` and `PLATFORMNAME.h` inside the `platform` directory. Define the above 4 functions inside `PLATFORMNAME.c`.

Once that is done, edit `platform.h` to check for the platform (use [this list](http://sourceforge.net/p/predef/wiki/OperatingSystems/) to see what flags are defined on what platforms) and conditionally include the `PLATFORMNAME.h` file (there's a commented block in `platform.h` that demonstrates how to do this).

Finally, edit `autopatch/SConscript` to detect the platform and correctly add the `PLATFORMNAME.c` file.

When all of this is done, you should be able to just `scons autopatch`.

# Cross compiling for 32 bit

(Soon)
