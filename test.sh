#!/bin/sh
set -e

root="$PWD"
mkdir -p .build
pushd .build

mkdir -p asymtab
pushd asymtab
cmake "$root/asymtab"
make
asymtab="$PWD/asymtab"
popd

mkdir -p autopatch
pushd autopatch
cmake -DPLATFORM=Linux $root
make
$asymtab gen elf test-executable/test
export AUTOPATCH_OPTIONS=debug
env LD_PRELOAD=autopatch/libautopatch.so AUTOPATCH_SYMTABS=test-executable/test.syms AUTOPATCH_LIBS=test-mod/libtest-mod.so test-executable/test
popd

popd
